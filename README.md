# py-obsw

Py-OBSW is a Python based software for developing service based applications. Py-OBSW can be used to perform
on-ground tests and quick prototyping for Spacecraft Onboard Software.