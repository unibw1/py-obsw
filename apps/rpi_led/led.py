from concurrent.futures import thread
from obsw.base.modal_component import BaseModalComponent
from apps.rpi_led.led_states import *
from obsw.modes.device_modes import BasicModeTypes
from obsw.modes.device_modes import BasicActions
import RPi.GPIO as GPIO
import threading
import time


class LED(BaseModalComponent):
    def __init__(self):
        super().__init__()
        self._modes = {
            BasicModeTypes.OFF.value: LEDOffMode(),
            BasicModeTypes.ON.value: LEDOnMode(),
            BasicModeTypes.NORMAL.value: LEDNormalMode(),
            BasicModeTypes.RAW.value: LEDRawMode(),
            BasicModeTypes.ERROR.value: LEDErrorMode(),
        }

        self._current_mode = self._modes[BasicModeTypes.OFF.value]
        self._default_mode = self._modes[BasicModeTypes.OFF.value]

        self._actions = {
            BasicActions.SWITCH_OFF.value: self.switch_off,
            BasicActions.SWITCH_ON.value: self.switch_on,
            BasicActions.SWITCH_NORMAL.value: self.switch_normal,
            BasicActions.SWITCH_RAW.value: self.switch_raw,
            BasicActions.SWITCH_ERROR.value: self.switch_error,
        }

        # set up led
        self.led_pin = 11  # PIN 11 Corresponds to GPIO17 on Rpi B+
        self.blink_delay = 0.5
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)
        GPIO.setup(self.led_pin, GPIO.OUT, initial=GPIO.LOW)

    def execute_action(self, action_id):
        assert self.is_object_configured(), "LED is not configured."
        print(f"executing action of id {action_id}")
        self.get_action(action_id)()

    def set_blink_delay(self, delay):
        self.blink_delay = delay

    def get_blink_delay(self):
        return self.blink_delay

    def switch_off(self):
        self._current_mode.switch_off(self)

    def switch_on(self):
        self._current_mode.switch_on(self)

    def switch_normal(self):
        self._current_mode.switch_normal(self)

    def switch_raw(self):
        self._current_mode.switch_raw(self)

    def switch_error(self):
        self._current_mode.switch_error(self)

    def turn_led_on(self):
        if not self.get_current_mode() == self.get_mode(
            BasicModeTypes.ON.value
        ) or not self.get_current_mode() == self.get_mode(BasicModeTypes.NORMAL.value):
            t = threading.Thread(target=self.__hardware_handler_blink, daemon=True)
            print("starting new thread...")
            t.start()

    def __hardware_handler_blink(self):
        print(f"Current Mode is now: {self.get_current_mode()}")
        while self.get_current_mode() == self.get_mode(
            BasicModeTypes.ON.value
        ) or self.get_current_mode() == self.get_mode(BasicModeTypes.NORMAL.value):
            GPIO.output(self.led_pin, GPIO.HIGH)
            time.sleep(self.get_blink_delay())
            GPIO.output(self.led_pin, GPIO.LOW)
            time.sleep(self.get_blink_delay())

    def turn_led_off(self):
        # any handling to be done before shutdown
        # should happen here
        self.__hardware_handler_off()

    def __hardware_handler_off(self):
        GPIO.output(self.led_pin, GPIO.LOW)
