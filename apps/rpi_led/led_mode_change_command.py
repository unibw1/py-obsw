from obsw.telecommand.mode_control_pus_telecommand import ModeControlPUSTelecommand
from obsw.base.punctual_action import ActionOutcome
from obsw.modes.device_modes import BasicModeTypes
from apps.rpi_led.led import LED


class LedModeControlPUSTelecommand(ModeControlPUSTelecommand):
    def __init__(self):
        super().__init__()
        
    def configure(self):
        self.set_modal_component(LED())

    def _do_action(self):
        assert self.is_object_configured(), "Mode change command is not configured."
        assert (
            self.modal_component.is_object_configured()
        ), "Modal Component is not configured."
        action = self.get_user_data()[0]
        blink_delay = int(self.get_user_data()[1])
        if action != BasicModeTypes.ON.value:
            self.modal_component.set_blink_delay(blink_delay)
            print(f'setting blink delay to {blink_delay}')
        print('executing modal action...')
        self.modal_component.execute_action(action)
        return ActionOutcome.ACTION_SUCCESS.value

    def is_object_configured(self):
        return super().is_object_configured()