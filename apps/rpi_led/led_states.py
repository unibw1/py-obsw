from obsw.modes.device_modes.error_mode import ErrorMode
from obsw.modes.device_modes.normal_mode import NormalMode
from obsw.modes.device_modes.off_mode import OffMode
from obsw.modes.device_modes.on_mode import OnMode
from obsw.modes.device_modes.raw_mode import RawMode
from obsw.modes.device_modes import BasicModeTypes


class LEDOffMode(OffMode):
    def __init__(self):
        super().__init__()

    def switch_on(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.ON.value)
        mode_based_component.set_blink_delay(1)
        print(f'Turning LED ON...')
        mode_based_component.turn_led_on()

    def switch_off(self, mode_based_component):
        pass

    def switch_normal(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.NORMAL.value)
        mode_based_component.turn_led_on()

    def switch_raw(self, mode_based_component):
        pass

    def switch_error(self, mode_based_component):
        pass


class LEDOnMode(OnMode):
    def __init__(self):
        super().__init__()

    def switch_on(self, mode_based_component):
        pass

    def switch_off(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.OFF.value)
        mode_based_component.turn_led_off()

    def switch_normal(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.NORMAL.value)
        mode_based_component.turn_led_on()

    def switch_raw(self, mode_based_component):
        pass

    def switch_error(self, mode_based_component):
        pass


class LEDNormalMode(NormalMode):
    def __init__(self):
        super().__init__()

    def switch_on(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.ON.value)
        mode_based_component.set_blink_delay(1)
        mode_based_component.turn_led_on()

    def switch_off(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.OFF.value)
        mode_based_component.turn_led_off()

    def switch_normal(self, mode_based_component):
        pass

    def switch_raw(self, mode_based_component):
        pass

    def switch_error(self, mode_based_component):
        pass


class LEDRawMode(RawMode):
    def __init__(self):
        super().__init__()

    def switch_on(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.ON.value)
        mode_based_component.set_blink_delay(1)
        mode_based_component.turn_led_on()

    def switch_off(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.OFF.value)
        mode_based_component.turn_led_off()

    def switch_normal(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.NORMAL.value)
        mode_based_component.turn_led_on()

    def switch_raw(self, mode_based_component):
        pass

    def switch_error(self, mode_based_component):
        pass


class LEDErrorMode(ErrorMode):
    def __init__(self):
        super().__init__()

    def switch_on(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.ON.value)
        mode_based_component.set_blink_delay(1)
        mode_based_component.turn_led_on()

    def switch_off(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.OFF.value)
        mode_based_component.turn_led_off()

    def switch_normal(self, mode_based_component):
        mode_based_component.set_current_mode(BasicModeTypes.NORMAL.value)
        mode_based_component.turn_led_on()

    def switch_raw(self, mode_based_component):
        pass

    def switch_error(self, mode_based_component):
        pass
