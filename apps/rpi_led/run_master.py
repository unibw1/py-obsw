from obsw.base.root import RootObject
from obsw.telecommand.telecommand_factory import TelecommandFactory
from obsw.telecommand.mode_control_pus_telecommand import ModeControlPUSTelecommand
from obsw.telecommand.doof_pus_telecommand import DoofPUSTelecommand
from obsw.telecommand.pus_telecommand import PUSTelecommand
from obsw.telemetry.telemetry_manager import TelemetryManager
from obsw.events.pus_event_repository import PUSEventRepository
from obsw.system.unix_clock import UnixClock
import socket
import logging
import time


logger = logging.getLogger(__package__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s : %(name)s : %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_change_mode_tc(mode_id):
    print("sending tc")


def run_app():
    # App initialisation
    logger.info("Initializing app...")
    logger.info("-" * 50)
    apid = 2**11 + 1 # apid contains secondary header flag
    slave_apid = 2**11 + 2
    # PUSTelecommand.set_apid(apid)

    # set data pool to dummy list (not using datapool atm)
    data_pool = [0]
    RootObject.set_data_pool(data_pool)

    # set parameter db to dummy (not using parameter db atm)
    parameter_db = [0]
    RootObject.set_parameter_db(parameter_db)

    # set max number of objects that can be created
    RootObject.set_system_list_size(50)

    # create clock
    logger.info("Initializing clock...")
    clock = UnixClock()

    # telemetry manager
    logger.info("Initializing TC Manager...")
    tlm = TelemetryManager()

    # set event repository
    logger.info("Initializing Evt Repo...")
    evt_list_size = 100
    evt_repo = PUSEventRepository()
    evt_repo.set_global_enable(True)
    evt_repo.set_repository_size(evt_list_size)
    evt_repo.set_clock(clock)
    evt_repo.set_telemetry_manager(tlm)
    RootObject.set_event_repository(evt_repo)

    # create doof command
    logger.info("Creating doof command...")
    tc_doof = DoofPUSTelecommand()
    tc_doof.set_time_tag(clock.get_time() + 1)
    PUSTelecommand.set_apid(slave_apid)
    tc_doof.set_source(apid)
    tc_doof_bytes = tc_doof.get_bytes()
    logger.debug(f"TC Doof Bytes : {tc_doof_bytes}")

    # create mode change command
    logger.info("Creating mode control command...")
    tc_mode_control = ModeControlPUSTelecommand()
    tc_mode_control.set_modal_component(1)
    tc_mode_control.set_raw_data_fast(bytearray([1, 0]))
    tc_mode_control.set_time_tag(clock.get_time() + 6)
    # tc_mode_control.set_apid(slave_apid)
    tc_mode_control.set_source(apid)
    tc_mode_control_bytes = tc_mode_control.get_bytes()
    # tc_mode_control_bytes.extend([1, 0])
    logger.debug(f'TC Mode Control Type: {tc_mode_control.get_type()}')
    logger.debug(f"TC Mode Control Bytes : {tc_mode_control_bytes}")

    logger.info(f"System is configured? : {RootObject.is_system_configured()}")

    HOST = socket.gethostname()
    PORT = 65432
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        logger.info(f"Waiting for connections on {(HOST, PORT)}")
        conn, addr = s.accept()
        with conn:
            logger.info(f"Client {addr} connected")
            try:
                while True:
                    # logger.info("Sending doof cmd data...")
                    # conn.sendall(tc_doof_bytes)
                    # time.sleep(10)
                    logger.info("Changing LED mode to ON...")
                    tc_mode_control_bytes[-1]=0 # blink rate is 0, i.e., default
                    tc_mode_control_bytes[-2]=1 # mode byte is 1
                    conn.sendall(tc_mode_control_bytes)
                    time.sleep(20)
                    logger.info("Changing LED mode to NORMAL...")
                    tc_mode_control_bytes[-1]=2 # blink rate is 1, i.e., configurable
                    tc_mode_control_bytes[-2]=2 # mode byte is 2
                    conn.sendall(tc_mode_control_bytes)
                    time.sleep(20)
                    # 
                    # send_tc()
            except KeyboardInterrupt:
                logger.info("Shutting down server and exiting.")
                conn.close()

if __name__ == "__main__":
    run_app()
