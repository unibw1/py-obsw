from obsw.base.root import RootObject
from obsw.events.pus_event_repository import PUSEventRepository
from obsw.telecommand.telecommand_manager import TelecommandManager
from obsw.telecommand.pus_telecommand_loader import PUSTelecommandLoader
from obsw.telecommand.pus_telecommand import PUSTelecommand
from obsw.telecommand.doof_pus_telecommand import DoofPUSTelecommand
from obsw.telecommand.mode_control_pus_telecommand import ModeControlPUSTelecommand
from obsw.telecommand.telecommand_factory import TelecommandFactory
from obsw.telemetry.telemetry_manager import TelemetryManager
from obsw.system.unix_clock import UnixClock
from apps.rpi_led.led_mode_change_command import LedModeControlPUSTelecommand
import logging
import socket
import time

logger = logging.getLogger(__package__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s : %(name)s : %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


def run_app():
    # App initialisation
    logger.info("Initializing app...")
    logger.info("-" * 50)
    apid = 2**11 + 2
    PUSTelecommand.set_apid(apid)

    # set data pool to dummy list (not using datapool atm)
    data_pool = [0]
    RootObject.set_data_pool(data_pool)

    # set parameter db to dummy (not using parameter db atm)
    parameter_db = [0]
    RootObject.set_parameter_db(parameter_db)

    # set max number of objects that can be created
    RootObject.set_system_list_size(50)

    # create clock
    logger.info("Initializing clock...")
    clock = UnixClock()

    # TC settings, size and number
    max_tc_num = 100  # max number of TCs allowed to be loaded
    max_tc_bytes = 1024  # max number of bytes in one TC packet
    # Intialize bytearray with first byte (denoting number of TCs to be loaded) as zero
    tc_buffer = bytearray([0])

    # create telecommand loader
    logger.info("Initializing TC Loader...")
    tcl = PUSTelecommandLoader()
    tcl.set_max_number_of_tc(max_tc_num)
    tcl.set_max_tc_pkt_length(max_tc_bytes)
    tcl.set_tc_buffer(tc_buffer)

    # create telecommand manager
    logger.info("Initializing TC Manager...")
    tcm = TelecommandManager()
    tcm.set_clock(clock)
    tcm.set_max_pending_command_size(int(max_tc_num / 2))

    # connect manager and loader
    logger.info("Connecting TC Manager and TC Loader...")
    tcl.set_tc_manager(tcm)
    tcm.set_tc_loader(tcl)

    # create telecommand instances
    tc_doof = DoofPUSTelecommand()
    tc_ledmode_change = LedModeControlPUSTelecommand()
    # configure tc mode change command to have a mode based component
    tc_ledmode_change.configure()

    # telemetry manager
    logger.info("Initializing TC Manager...")
    tlm = TelemetryManager()

    # set event repository
    logger.info("Initializing Evt Repo...")
    evt_list_size = 100
    evt_repo = PUSEventRepository()
    evt_repo.set_global_enable(True)
    evt_repo.set_repository_size(evt_list_size)
    evt_repo.set_clock(clock)
    evt_repo.set_telemetry_manager(tlm)
    RootObject.set_event_repository(evt_repo)

    # create telecommand factory
    logger.info("Initializing TC Factory...")
    tc_factory = TelecommandFactory.get_instance()
    tc_factory.set_doof_telecommand(0, tc_doof)
    tc_factory.set_mode_change_telecommand(0, tc_ledmode_change)
    
    # check configurations
    logger.info("-" * 50)
    logger.info(f"TC Manager is configured? : {tcm.is_object_configured()}")
    logger.info(f"TC Loader is configured? : {tcl.is_object_configured()}")
    logger.info(f"TL Manager is configured? : {tlm.is_object_configured()}")
    logger.info(f"Evt Repo is configured? : {evt_repo.is_object_configured()}")
    logger.info(f"DoofTC is configured? : {tc_doof.is_object_configured()}")
    logger.info(f"TC Factory is configured? : {tc_factory.is_object_configured()}")
    logger.info(f"System is configured? : {RootObject.is_system_configured()}")
    logger.info("-" * 50)

    logger.info("setting up TCP client...")
    HOST = "192.168.1.140"
    PORT = 65432
    data = None

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        try:
            while True:
                data = s.recv(1024)
                if data:
                    logger.info(f"Received TC from GS : {data}")
                    tc_buffer[0]=1
                    if len(tc_buffer) > 1:
                        tc_buffer[1:] = data
                    else:
                        tc_buffer.extend(data)
                    tc_buffer[1:] = data
                    tcl.load()
                    tcm.execute()
        except KeyboardInterrupt:
            logger.info("Shutting down connection.")
            s.close()


if __name__ == "__main__":
    run_app()
