import enum


class TMType(enum.Enum):
    PUS_TM_PACKET = 0
    SERANIS_PLD_TM_PACKET = 1


class TMSubType(enum.Enum):
    PUS_TM_PACKET = 0
    SERANIS_PLD_TM_PACKET = 1
